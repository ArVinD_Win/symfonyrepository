<?php
// src/Controller/GetRepositoriesController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Github\Client;

/**
 * Class GetRepositoriesController for fetching repositories from source.
 */
class GetRepositoriesController extends AbstractController
{
	/**
     * Fetch symfony repositories.
     * @return html response
     *
     */
    public function getRepository()
    {
		$client = new Client();
		$symfony_repo = $client->api('users')->repositories('symfony');
		
		$resultRepo = [];
		$i = 0;
		foreach ($symfony_repo as $repo)
		{
			$resultRepo[$i]['id'] = $repo['id'];
			$resultRepo[$i]['name'] = $repo['name'];
			$resultRepo[$i]['html_url'] = $repo['html_url'];
			$resultRepo[$i]['description'] = $repo['description'];
			$resultRepo[$i]['watchers'] = $repo['watchers'];
			$resultRepo[$i]['open_issues'] = $repo['open_issues'];
			$resultRepo[$i]['forks'] = $repo['forks'];
			$i++;
		}
        return $this->render('gitRepositories/get.html.twig', [
            'symfonyRepo' => $resultRepo,
        ]);
    }
}